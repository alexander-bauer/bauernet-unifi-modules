variable "namespace" {
  type = string
}

variable "unifi_fqdn" {
  type = string
}

variable "storageclass" {
  type = string
}

variable "chart_version" {
  type    = string
  default = null
}

variable "service_sharing_key" {
  type = string
}

variable "domain" {
  type = string
}


resource "kubernetes_namespace" "unifi" {
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "unifi" {
  name       = "unifi"
  repository = "https://k8s-at-home.com/charts/"
  chart      = "unifi"
  namespace  = one(kubernetes_namespace.unifi.metadata).name
  version    = var.chart_version

  values = [
    yamlencode({
      hostname = "unifi"
    }),
    yamlencode({
      service = {
        main = {
          enabled = true
          type    = "LoadBalancer"
          annotations = var.service_sharing_key != null ? {
            "metallb.universe.tf/allow-shared-ip" : var.service_sharing_key
          } : {}
        }
      }
    }),
    yamlencode({
      ingress = {
        main = {
          enabled = true
        }
      }
    }),
    yamlencode({
      persistence = {
        data = {
          enabled      = true
          accessMode   = "ReadWriteOnce"
          size         = "5Gi"
          storageClass = var.storageclass
        }
      }
    }),
  ]
}
