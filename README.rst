######################
BauerNet Unifi Modules
######################

This repository contains Terraform modules for configuring the Unifi Controller in the
BauerNet infrastructure.

.. toctree::
   :maxdepth: 1

   */README
   CHANGELOG
